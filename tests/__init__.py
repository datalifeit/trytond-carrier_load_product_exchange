# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_carrier_load_product_exchange import suite

__all__ = ['suite']
