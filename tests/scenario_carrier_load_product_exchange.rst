======================================
Carrier Load Product Exchange Scenario
======================================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts, create_tax
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> from trytond.modules.stock_unit_load.tests.tools import create_unit_load
    >>> from proteus import Model, Wizard
    >>> today = datetime.date.today()

Install carrier_load_product_exchange::

    >>> config = activate_modules(['carrier_load_product_exchange', 
    ...     'carrier_load_done2running', 'stock_party_warehouse'])

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> tax = create_tax(Decimal('.10'))
    >>> tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.customer_taxes.append(tax)
    >>> account_category_tax.save()

Get warehouse and dock::

    >>> Location = Model.get('stock.location')
    >>> wh, = Location.find([('type', '=', 'warehouse')])
    >>> dock = wh.docks.new()
    >>> dock.name = 'Dock 1'
    >>> dock.code = 'D1'
    >>> wh.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer_loc = Location(type='customer', name='Customer 1')
    >>> customer_loc.save()
    >>> customer.customer_location = customer_loc
    >>> customer.create_warehouse = True
    >>> customer.save()

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Create carrier::

    >>> Carrier = Model.get('carrier')
    >>> Uom = Model.get('product.uom')
    >>> Template = Model.get('product.template')
    >>> carrier = Carrier()
    >>> unit, = Uom.find([('name', '=', 'Unit')], limit=1)
    >>> transport_template = Template(
    ...     name='Transport',
    ...     type='service',
    ...     list_price=Decimal(500),
    ...     cost_price=Decimal(0),
    ...     default_uom=unit)
    >>> transport_template.save()
    >>> party_carrier = Party(name='Carrier 1')
    >>> party_carrier.save()
    >>> carrier.party = party_carrier
    >>> carrier.carrier_product = transport_template.products[0]
    >>> carrier.save()
    >>> carrier_party = carrier.party
    >>> carrier_loc = Location(type='supplier', name='Carrier 1')
    >>> carrier_loc.save()
    >>> carrier_party.supplier_location = carrier_loc
    >>> carrier_party.save()
    >>> carrier_product = carrier.carrier_product.template
    >>> carrier_product.purchasable = True
    >>> carrier_product.purchase_uom = carrier_product.default_uom
    >>> carrier_product.account_category = account_category_tax
    >>> carrier_product.save()

Add other products to unit load::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> ProductCategory = Model.get('product.category')
    >>> category = ProductCategory(name='Category')
    >>> category.save()
    >>> product = Product()
    >>> template = ProductTemplate()
    >>> template.name = 'Plastic Case 30x30'
    >>> template.categories.append(category)
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category_tax
    >>> template.save()
    >>> product, = template.products
    >>> product.cost_price = Decimal('5')
    >>> product.save()

Configure default location::

    >>> storage, = Location.find([('code', '=', 'STO')])
    >>> cat_loc = storage.categories.new()
    >>> cat_loc.category = category
    >>> storage.save()

Configure carrier configuration::

    >>> Configuration = Model.get('carrier.configuration')
    >>> configuration = Configuration(1)
    >>> configuration.product_exchange_category = category
    >>> configuration.exchange_quantity
    'propose'
    >>> configuration.save()

Create unit loads::

    >>> Unitload = Model.get('stock.unit_load')
    >>> uls = []
    >>> main_product = None
    >>> for x in range(0, 7):
    ...     ul = create_unit_load(config=config, do_state=False, product=main_product,
    ...         default_values={'start_date': datetime.datetime.now() + relativedelta(days=-1)})
    ...     move = ul.moves.new()
    ...     move.planned_date = today
    ...     move.product = product
    ...     move.quantity = Decimal(2)
    ...     move.from_location = ul.moves[0].from_location
    ...     move.to_location = ul.moves[0].to_location
    ...     move.currency = move.company.currency
    ...     move.unit_price = product.cost_price
    ...     ul.save()
    ...     uls.append(ul)
    ...     if main_product is None:
    ...         main_product = ul.product
    >>> template = main_product.template
    >>> template.salable = True
    >>> template.account_category = account_category_tax
    >>> template.save()

Create sale::

    >>> Sale = Model.get('sale.sale')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.invoice_method = 'order'
    >>> sale.warehouse = wh
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = main_product
    >>> sale_line.quantity = 70.0
    >>> sale_line.ul_quantity = 2.0
    >>> sale_line = sale.lines.new()
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale_line = sale.lines.new()
    >>> sale_line.product = main_product
    >>> sale_line.quantity = 140.0
    >>> sale_line.ul_quantity = 4.0
    >>> sale.click('quote')

Start loading::

    >>> sale.click('confirm')
    >>> start_load = Wizard('carrier.load.create_wizard', [sale])
    >>> start_load.form.dock = sale.warehouse.docks[0]
    >>> start_load.form.carrier = carrier
    >>> start_load.form.vehicle_number = 'MX4459'
    >>> start_load.form.lines[1].ul_quantity = Decimal(1)
    >>> start_load.execute('pre_load_')

Check load order::

    >>> Load = Model.get('carrier.load')
    >>> Loadorder = Model.get('carrier.load.order')
    >>> sale.reload()
    >>> load_order = Loadorder(sale.loads[0].id)
    >>> load = load_order.load
    >>> load_order.click('wait')
    >>> loading = Wizard('carrier.load.create_wizard', [sale])
    >>> loading.form.load_order = load_order
    >>> loading.form.lines[0].ul_quantity = Decimal(1)
    >>> loading.execute('pre_load_')
    >>> load_order.reload()
    >>> sale.reload()
    >>> loading = Wizard('carrier.load.create_wizard', [sale])
    >>> loading.form.load_order = load_order
    >>> loading.form.lines[0].ul_quantity = Decimal(3)

Start loading ULs::

    >>> for ul in uls:
    ...     ul.click('assign')
    ...     ul.click('do')
    >>> start_load = Wizard('carrier.load_uls', [load_order])
    >>> start_load.form.ul_code = uls[0].code
    >>> start_load.execute('load_')
    >>> start_load.form.ul_code = uls[1].code
    >>> start_load.execute('load_')
    >>> start_load.form.ul_code = uls[2].code
    >>> start_load.execute('load_')
    >>> start_load.form.ul_code = uls[3].code
    >>> start_load.execute('load_')
    >>> start_load.form.loaded_uls
    4

One move is created and added to the exchange_moves::

    >>> len(load_order.exchange_moves)
    1
    >>> move, = load_order.exchange_moves

Check move values::

    >>> Location = Model.get('stock.location')
    >>> storage, = Location.find([('code', '=', 'STO')])
    >>> move.quantity
    8.0
    >>> move.uom == template.default_uom
    True
    >>> move.to_location == storage
    True
    >>> move.from_location == customer_loc
    True

Check exchange moves must be done::

    >>> load_order.click('do')
    >>> move.reload()
    >>> move.state
    'done'

Check outgoing_moves::

    >>> move = load_order.outgoing_moves[0]
    >>> move.to_location == customer_loc
    True

Check returnables moves::

    >>> rmove, = load_order.returnables_moves
    >>> rmove.origin == load_order.exchange_moves[0]
    True
    >>> rmove.from_location == customer.warehouse.storage_location
    True
    >>> rmove.to_location == customer_loc
    True

Check exchange moves state when reverting to running::

    >>> load_order.click('run')
    >>> load_order.reload()
    >>> move, = load_order.exchange_moves
    >>> move.state
    'draft'
    >>> len(load_order.returnables_moves)
    0

Check exchange is removed when unloading ULs::

    >>> ul = uls[0]
    >>> start_load = Wizard('carrier.load_uls', [load_order])
    >>> start_load.form.uls_loaded.append(ul)
    >>> start_load.execute('unload_')
    >>> load_order.reload()
    >>> len(load_order.exchange_moves)
    1
    >>> load_order.exchange_moves[0].quantity
    6.0
    >>> load_order.reload()
    >>> for ul in uls[1:4]:
    ...     ul.click('unload')
    >>> load_order.reload()
    >>> len(load_order.exchange_moves)
    0

Configure carrier configuration::

    >>> configuration.exchange_quantity = 'empty'
    >>> configuration.save()

Create sale2::

    >>> sale2 = Sale()
    >>> sale2.party = customer
    >>> sale2.payment_term = payment_term
    >>> sale2.invoice_method = 'order'
    >>> sale2.warehouse = wh
    >>> sale_line = sale2.lines.new()
    >>> sale_line.product = main_product
    >>> sale_line.quantity = 70.0
    >>> sale_line.ul_quantity = 2.0
    >>> sale_line = sale2.lines.new()
    >>> sale_line.type = 'comment'
    >>> sale_line.description = 'Comment'
    >>> sale_line = sale2.lines.new()
    >>> sale_line.product = main_product
    >>> sale_line.quantity = 140.0
    >>> sale_line.ul_quantity = 4.0
    >>> sale2.click('quote')

Start loading2::

    >>> sale2.click('confirm')
    >>> start_load2 = Wizard('carrier.load.create_wizard', [sale2])
    >>> start_load2.form.dock = sale.warehouse.docks[0]
    >>> start_load2.form.carrier = carrier
    >>> start_load2.form.vehicle_number = 'MX4459'
    >>> start_load2.form.lines[1].ul_quantity = Decimal(1)
    >>> start_load2.execute('pre_load_')


Start loading ULs::

    >>> for ul in uls:
    ...     ul.click('assign')
    ...     ul.click('do')
    >>> load_order2 = Loadorder(sale2.loads[0].id)
    >>> start_load2 = Wizard('carrier.load_uls', [load_order2])
    >>> start_load2.form.ul_code = uls[4].code
    >>> start_load2.execute('load_')
    >>> start_load2.form.ul_code = uls[5].code
    >>> start_load2.execute('load_')
    >>> start_load2.form.ul_code = uls[6].code
    >>> start_load2.execute('load_')
    >>> start_load2.form.loaded_uls
    3


Check move values::

    >>> len(load_order2.exchange_moves)
    1
    >>> move2, = load_order2.exchange_moves
    >>> move2.quantity
    0.0
   

Check exchange moves must be done and load_order::

    >>> load_order2.click('do') # doctest: +ELLIPSIS
    Traceback (most recent call last):
    ...
    trytond.exceptions.UserError: The quantity of exchange movement "..." of Load order "..." cannot be zero. - 
    >>> move2.quantity = Decimal(2)
    >>> move2.save()
    >>> load_order2.click('do')
    >>> move2.reload()
    >>> move2.state
    'done'
    >>> load_order2.state
    'done'

