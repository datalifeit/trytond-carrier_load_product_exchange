# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import load
from . import configuration
from . import stock


def register():
    Pool.register(
        configuration.Configuration,
        load.LoadOrder,
        load.CMRTemplate,
        stock.Move,
        stock.UnitLoad,
        module='carrier_load_product_exchange', type_='model')
    Pool.register(
        load.LoadOrder2,
        depends=['carrier_load_done2running'],
        module='carrier_load_product_exchange', type_='model')
    Pool.register(
        load.CMR,
        load.RoadTransportNote,
        module='carrier_load_product_exchange', type_='report')
    Pool.register(
        load.LoadOrderReturnable,
        module='carrier_load_product_exchange', type_='model',
        depends=['stock_party_warehouse'])
    Pool.register(
        load.LoadOrderReturnable2,
        module='carrier_load_product_exchange', type_='model',
        depends=['stock_party_warehouse', 'carrier_load_done2running'])
    Pool.register(
        configuration.Configuration2,
        stock.LoadOrder,
        stock.ModifyExchangeMoveStart,
        module='carrier_load_product_exchange', type_='model',
        depends=['stock_move_done2cancel'])
    Pool.register(
        stock.ModifyExchangeMove,
        module='carrier_load_product_exchange', type_='wizard',
        depends=['stock_move_done2cancel'])
