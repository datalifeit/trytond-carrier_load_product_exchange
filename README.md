datalife_carrier_load_product_exchange
======================================

The carrier_load_product_exchange module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-carrier_load_product_exchange/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-carrier_load_product_exchange)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
