# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta, Pool
from trytond.model import ModelView, fields
from trytond.pyson import Or, Not, Equal, Eval
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.transaction import Transaction
from trytond.exceptions import UserWarning
from trytond.i18n import gettext


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    @classmethod
    def _get_origin(cls):
        models = super(Move, cls)._get_origin()
        models.append('carrier.load.order')
        return models


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    @classmethod
    def unload(cls, records, load_order=None):
        orders = {}
        for record in records:
            if not record.load_order:
                continue
            orders.setdefault(record.load_order, []).append(record)

        super().unload(records, load_order)

        for order, uls in orders.items():
            for ul in uls:
                order.update_exchange_moves_from_ul(ul, add=False)


class LoadOrder(metaclass=PoolMeta):
    __name__ = 'carrier.load.order'

    @classmethod
    def __setup__(cls):
        super().__setup__()
        cls._buttons.update({
            'modify_exchange_moves': {
                'invisible': Or(
                    Not(Equal(Eval('state'), 'done')),
                    Not(Eval('exchange_moves'))),
                'depends': ['state', 'exchange_moves']
            }
        })

    def get_exchange_quantity_error(self, exchange_move):
        pool = Pool()
        Warning = pool.get('res.user.warning')
        Conf = pool.get('carrier.configuration')
        conf = Conf(1)

        if conf.exchange_quantity == 'empty_warn':
            warning_name = 'exchange_move_quantity_%s_%s' % (
                exchange_move.id, self.id)
            if Warning.check(warning_name):
                raise UserWarning(warning_name,
                    gettext('carrier_load_product_exchange.'
                        'msg_carrier_load_order_exchange_moves_quantity',
                        move=exchange_move.rec_name,
                        order=self.rec_name))
        else:
            super().get_exchange_quantity_error(exchange_move)

    @classmethod
    @ModelView.button_action(
        'carrier_load_product_exchange.wizard_modify_exchange_moves')
    def modify_exchange_moves(cls, records):
        pass


class ModifyExchangeMoveStart(ModelView):
    """Modify Exchange Move Start"""
    __name__ = 'carrier.load.order.modify_exchange_move.start'

    exchange_moves = fields.One2Many('stock.move', None, 'Exchange moves',
        readonly=True)


class ModifyExchangeMove(Wizard):
    """Modify Exchange Move"""
    __name__ = 'carrier.load.order.modify_exchange_move'
    start_state = 'pre_start'

    start = StateView(
        'carrier.load.order.modify_exchange_move.start',
        'carrier_load_product_exchange.modify_exchange_move_start_view_form',
        [Button('Cancel', 'end_', 'tryton-cancel'),
         Button('Modify', 'modify', 'tryton-ok', default=True)])

    end_ = StateTransition()
    pre_start = StateTransition()
    modify = StateTransition()

    def transition_pre_start(self):
        pool = Pool()
        Move = pool.get('stock.move')

        with Transaction().set_context(check_origin=False):
            moves = [m for r in self.records for m in r.exchange_moves]
            Move.cancel(moves)
            Move.draft(moves)
        return 'start'

    def default_start(self, fields):
        return {
            'exchange_moves': list(map(int, [m
                for r in self.records for m in r.exchange_moves]))
        }

    def transition_modify(self):
        pool = Pool()
        Move = pool.get('stock.move')

        Move.save(self.start.exchange_moves)
        Move.do(self.start.exchange_moves)
        return 'end'

    def transition_end_(self):
        pool = Pool()
        Move = pool.get('stock.move')

        Move.do(self.start.exchange_moves)
        return 'end'
